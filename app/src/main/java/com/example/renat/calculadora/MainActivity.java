package com.example.renat.calculadora;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private String memoria = "";
    private String numero = "";
    private String operadores = "";
    private ArrayList<Float> numeros = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void clickNumber(View view) {
        //Obtendo o valor do btn
        Button btnClick = (Button) view;
        this.memoria += btnClick.getText();

        //Salvando na list de números
        this.numero += btnClick.getText();

        //Mostrando a memória no visor
        TextView visorMemo = findViewById(R.id.visorMemo);
        visorMemo.setText(this.memoria);
    }

    public void clickOperator(View view){

        if(this.numero.equals("")){
            return;
        }
        //Obtendo o valor do btn
        Button btnClick = (Button)view;
        this.memoria += btnClick.getText();

        //Salvando na list de operações
        this.operadores += btnClick.getText();

        //Mostrando a memória no visor
        TextView visorMemoria = findViewById(R.id.visorMemo);
        visorMemoria.setText(this.memoria);

        //Salvando o último valor digitado no vetor de número
        this.numeros.add(Float.parseFloat(this.numero));
        this.numero = "";
    }

    public void clickEquals(View view){
        if(this.memoria.charAt(this.memoria.length()-1) <48 || this.memoria.charAt(this.memoria.length()-1) >57  ){
            Toast.makeText(MainActivity.this, "Formato inválido!", Toast.LENGTH_SHORT).show();
            return;
        }

        //Salvando o último valor digitado no vetor de número
        this.numeros.add(Float.parseFloat(this.numero));
        this.numero = "";

        //Recupera a quantidade operadores digitados
        int qtdeOperators = this.operadores.length();

        //Realiza as operações
        float result = this.numeros.get(0).floatValue();
        for(int i=0;i<qtdeOperators;i++){
            try {
                result = this.calc(result, this.numeros.get(i + 1).floatValue(), this.operadores.charAt(i));
            }catch(ArithmeticException e){
                Toast.makeText(MainActivity.this, "Divisão por 0 impossivel!", Toast.LENGTH_SHORT).show();
                this.numeros.remove(i+1);
                this.numero = "0";
                return;
            }
        }

        //Seta os resultados no visor
        TextView visorResultado = findViewById(R.id.visorResult);
        visorResultado.setText(result + "");
        TextView visorMemoria = findViewById(R.id.visorMemo);
        visorMemoria.setText("");

        //Reseta as variaveis
        this.memoria = result + "";
        this.numero = "" + result;
        this.operadores = "";
        this.numeros.clear();
    }

    public float calc(float value1, float value2, char operator){

        switch(operator) {
            case '+':
                return value1 + value2;
            case '-':
                return value1 - value2;
            case '/':
                if(value2 == 0)
                    throw new ArithmeticException();
                return value1 / value2;
            case 'x':
                return value1 * value2;
            default:
                return 0;
        }
    }



    public void clickClear(View view){
        //Limpando os visores
        TextView visorMemoria = findViewById(R.id.visorMemo);
        TextView visorResultado = findViewById((R.id.visorResult));
        visorMemoria.setText("");
        visorResultado.setText("");

        //Limpando as váriaveis de cálculo
        this.memoria = "";
        this.numero = "";
        this.operadores = "";
        this.numeros.clear();
    }
}